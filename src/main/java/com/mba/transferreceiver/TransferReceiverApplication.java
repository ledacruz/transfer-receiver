package com.mba.transferreceiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransferReceiverApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransferReceiverApplication.class, args);
	}

}
