package com.mba.transferreceiver.model;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder

public class TransferReceiver {

    public String eventType;
    public String timestamp;
}
