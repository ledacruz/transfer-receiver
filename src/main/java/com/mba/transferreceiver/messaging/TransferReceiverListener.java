package com.mba.transferreceiver.messaging;

import com.google.gson.Gson;
import com.mba.transferreceiver.model.TransferReceiver;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TransferReceiverListener {

    private final Logger logger = LoggerFactory.getLogger(TransferReceiverListener.class);



    @KafkaListener(topics = "orchestration-topic", groupId = "group-transfer-receiver")
    public void consumeChoreography(String message){

        TransferReceiver transferReceiver = convertStringtoTransferReceiver(message);
        logger.info(String.format("$$ -> Consumed Message -> %s", transferReceiver.getEventType() + " " + transferReceiver.getTimestamp()));
    }

    @KafkaListener(topics = "choreography-topic", groupId = "group-transfer-receiver")
    public void consumeOrchestration(String message){

        TransferReceiver transferReceiver = convertStringtoTransferReceiver(message);
        logger.info(String.format("$$ -> Consumed Message -> %s", transferReceiver.getEventType() + " " + transferReceiver.getTimestamp()));
    }

    @KafkaListener(topics = "orchestrator-topic", groupId = "group-transfer-receiver")
    public void consumeOrchestrator(String message){

        TransferReceiver transferReceiver = convertStringtoTransferReceiver(message);
        logger.info(String.format("$$ -> Consumed Message -> %s", transferReceiver.getEventType() + " " + transferReceiver.getTimestamp()));
    }


    private TransferReceiver convertStringtoTransferReceiver(String message){
        Gson gson = new Gson();
        TransferReceiver transferReceiver = gson.fromJson(message, TransferReceiver.class);

        return transferReceiver;

    }
}
